# On the statistical relation between the halo mass function and the internal structure of dark matter haloes
## Associated codes

The notebook in this repository aims at providing base codes to perform the transformations of the halo mass function as well as sparsity predictions presented in Richardson & Corasaniti (2023) [https://arxiv.org/abs/2212.03233 ]. These codes are made free to use by the community under the condition that any publication that makes use of these codes considers citing the associated publication. That would be really cool, Thanks a bunch!

# HMF and sparsity data

The data in the hdf5 files associated to this notebook is extracted from the Uchuu simulation (Ishiyama et al. 2021) [https://arxiv.org/abs/2007.14720 ]. The halo catalogues from which the data is produced can be found on the skies and universes database http://skiesanduniverses.org/Simulations/Uchuu/ which we highly recommend visiting. If you want to use any of this data please consider acknowledging the simulations by adding this acknowledgement to your work:

"We thank Instituto de Astrofisica de Andalucia (IAA-CSIC), Centro de Supercomputacion de Galicia (CESGA) and the Spanish academic and research network (RedIRIS) in Spain for hosting Uchuu DR1 and DR2 in the Skies & Universes site for cosmological simulations. The Uchuu simulations were carried out on Aterui II supercomputer at Center for Computational Astrophysics, CfCA, of National Astronomical Observatory of Japan, and the K computer at the RIKEN Advanced Institute for Computational Science. The Uchuu DR1 and DR2 effort has made use of the skun@IAA_RedIRIS and skun6@IAA computer facilities managed by the IAA-CSIC in Spain (MICINN EU-Feder grant EQC2018-004366-P)."

And optionally

"We thank Tamara R.G. Richardson for providing her measurements of the halo mass function and sparsity distributions from the Uchuu simulation used within this work."

## Dependencies

<ul>
    <li> numpy
    <li> scipy
    <li> matplotlib
    <li> tqdm
    <li> h5py
    <li> colossus
</ul>